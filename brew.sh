#!/usr/bin/env bash

# Install command-line tools using Homebrew.

# Make sure we’re using the latest Homebrew.
brew update

# Upgrade any already-installed formulae.
brew upgrade

# Save Homebrew’s installed location.
BREW_PREFIX=$(brew --prefix)

# Install GNU core utilities (those that come with macOS are outdated).
# Don’t forget to add `$(brew --prefix coreutils)/libexec/gnubin` to `$PATH`.
brew install coreutils
ln -s "${BREW_PREFIX}/bin/gsha256sum" "${BREW_PREFIX}/bin/sha256sum"

# Install some other useful utilities like `sponge`.
brew install moreutils
# Install GNU `find`, `locate`, `updatedb`, and `xargs`, `g`-prefixed.
brew install findutils
# Install GNU `sed`, overwriting the built-in `sed`.
brew install gnu-sed --with-default-names
# Install a modern version of Bash.
brew install bash
brew install bash-completion2

# Switch to using brew-installed bash as default shell
if ! fgrep -q "${BREW_PREFIX}/bin/bash" /etc/shells; then
  echo "${BREW_PREFIX}/bin/bash" | sudo tee -a /etc/shells;
  chsh -s "${BREW_PREFIX}/bin/bash";
fi;

# Install more recent versions of some macOS tools.
brew install grep
brew install openssh
brew install screen

# Casks
brew install --cask 1password
brew install --cask alfred
brew install --cask bartender
brew install --cask bitwarden
brew install --cask google-cloud-sdk
brew install --cask iterm2
brew install --cask istat-menus
brew install --cask microsoft-office
brew install --cask microsoft-teams
brew install --cask phpstorm
brew install --cask postman
brew install --cask rectangle
brew install --cask spotify
brew install --cask visual-studio-code

brew install colima
brew install docker
brew install git
brew install httpie
brew install jump
brew install jq
brew install kubectx
brew install node@14
brew install pyenv
brew install pipx
brew install sops
brew install terraform
brew install terragrunt
brew install tree
brew install yq

# Remove outdated versions from the cellar.
brew cleanup
